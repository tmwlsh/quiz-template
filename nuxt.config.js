const pkg = require('./package')

module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  router: {
   base: '/campaigns/horse-generator/'
  },
  head: {
    title: 'Grand National Horse Generator',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description },
      { property: "og:url", content: ""},
      { property: "og:type", content: "article"},
      {
        property: "og:title",
        content: "Grand National Horse Generator "},
      {
        property: "og:description",
        content: "Don’t know who to back at the Grand National? This generator has got you covered."
      },
      {
        property: "og:image",
        content: "https://blog.betvictor.com/campaigns/horse-generator/social-share-image.jpg"
      },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    'assets/sass/globals.sass',
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [{
    src: '~/plugins/vue-slick',
    ssr: false
  },
  {
    src: '~/plugins/confetti',
    ssr: false
  }],

  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/dotenv'
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      config.node = {
        fs: 'empty'
      }
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
