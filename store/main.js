import appData from '../assets/data.json';
import axios from 'axios';
import _ from 'lodash';

export const state = () => ({
  appData,
  userAnswers: [],
  currentSection: 'splash',
  currentQuestionIndex: 1,
});

export const mutations = {
  setUserAnswer(state, payload) {
    const match = state.userAnswers.find(answer => {
      return answer.questionSlug === payload.questionSlug;
    });
    if(!match) state.userAnswers.push(payload);
  },
  setSection(state, payload) {
    state.currentSection = payload;
  },

  setResults(state, payload) {
    let data = payload
    if (payload === 'error') {
      data = appData.results;
      console.log(data);
    }
    state.appData.results = data.map(result => ({
      name: result.name,
      jockeyName: result.jockeyName,
      trainerName: result.trainerName,
      horseNumber: result.horseNumber,
      runDate: result.runDate,
      odds: result.odds,
      answers: [
        {
          questionSlug: 'risk',
          answerSlug: result.risk
        },
        {
          questionSlug: 'horseAge',
          answerSlug: result.horseAge,
        },
        {
          questionSlug: 'favouriteThing',
          answerSlug: result.favouriteThing
        },
        {
          questionSlug: 'colour',
          answerSlug: result.colour
        },
      ]
    }));
  },

  setQuestions(state, payload) {
    let data = payload
    if(payload === 'error') {
      data = appData.questions;
    }
    const groupedBy = _.groupBy(data, 'questionSlug');
    const questions = [];
    Object.keys(groupedBy).forEach((slug) => {
      const dataObj = data.filter(question => {
        return question.questionSlug === slug;
      });
      questions.push({
        questionSlug: slug,
        questionName: dataObj[0].questionName,
        answers: dataObj,
      })
    });
    state.appData.questions = questions;
  },


  reset(state) {
    state.currentSection = 'splash';
    state.userAnswers = [];
    state.currentQuestionIndex = 1;
  },

  incrementCurrentQuestionIndex(state) {
    state.currentQuestionIndex++;
  }

};
